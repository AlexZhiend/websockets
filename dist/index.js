"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _express = _interopRequireDefault(require("express"));

var _http = _interopRequireDefault(require("http"));

var _clienteWS = _interopRequireDefault(require("./routes/clienteWS"));

var _middleware = _interopRequireDefault(require("./libs/middleware"));

// import '@babel/polyfill'
// Importando rutas
// Importando configuraciones de la BD y middleware
var PORT = process.env.PORT || 3000; // const HOST='192.168.0.29'

var app = (0, _express["default"])();
(0, _middleware["default"])(app);

var server = _http["default"].Server(app); // WEB SOCKETS SERVER CLIENTE


(0, _clienteWS["default"])(app, server); // server listening

server.listen(PORT, function () {
  console.log("Aplicacion corriendo en puerto:", PORT);
});