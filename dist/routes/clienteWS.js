"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ws = _interopRequireDefault(require("ws"));

var _url = _interopRequireDefault(require("url"));

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

// SE TIENE QUE VER EL CASO EN EL QUE EL SOCKET NO ESTA ABIERTO
// SE PUEDE ESPERAR MENSAJES POR EL WEBSOCKET CON EL EVENTO:
// wssClient.on('message', callbackFunc)
var _default = function _default(app, server) {
  var socketsArray = [];
  var wssClient = new _ws["default"].Server({
    server: server,
    path: '/client'
  });

  function noop() {}

  function heartbeat() {
    this.isAlive = true;
  }

  setInterval(function ping() {
    wssClient.clients.forEach(function (ws) {
      if (!ws.isAlive) {
        var socketTodos = Object.values(userSockets);
        var arrIds = Object.keys(userSockets);
        var idx = socketTodos.indexOf(ws);
        var idxSocket = arrIds[idx];
        delete userSockets[idxSocket];
        console.log('socket con index ' + idx + 'fue quitado de userSockets ' + Object.keys(userSockets));
        return ws.terminate();
      }

      ws.isAlive = false;
      ws.ping(noop);
    });
  }, 10000);
  var userSockets = {};
  wssClient.on('connection', function (socket, request) {
    socket.isAlive = true;
    socket.on('pong', heartbeat);

    var parameters = _url["default"].parse(request.url, true);

    var userId = parameters.query.user_id;
    userSockets[userId] = socket;
    console.log('Conexion establecida para usuario', userId);
    socketsArray.push(userId);
  });
  app.route('/notificacion/:id/:hcl').get(function (req, res) {
    var hcl = req.params.hcl;
    var id = req.params.id;
    console.log('api llamada');
    console.log(hcl); // let socket = userSockets[id]

    console.log(socketsArray.length);

    for (var i = 0; i < socketsArray.length; i++) {
      var socket = userSockets[socketsArray[i]];

      if (typeof socket !== 'undefined' && socket.readyState === _ws["default"].OPEN) {
        socket.send(JSON.stringify({
          hcl: hcl
        }));
        console.log("enviado a:" + i);
        console.log(socket);
        res.json({
          hola: 'holaa'
        });
      } else {
        res.json({
          hola: 'error'
        });
        res.status(204).json({
          mensaje: 'exito'
        });
      }
    } // userSockets.forEach(data => {
    //   if (typeof (data) !== 'undefined' && data.readyState === ws.OPEN) {
    //     data.send(JSON.stringify({hcl:hcl}))
    //     // userSockets.forEach(data => {
    //     //   data.send(JSON.JSON.stringify({hcl:hcl}))
    //     // });
    //     res.json({hola:'holaa'})
    //   } else {     
    //     res.json({hola:'error'})   
    //     res.status(204).json({ mensaje: 'exito' })
    //   }
    // });

  });
};

exports["default"] = _default;