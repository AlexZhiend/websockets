"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _cors = _interopRequireDefault(require("cors"));

module.exports = function (app) {
  var allowedOrigins = ['localhost:8080', 'http://165.227.57.22', 'http://165.227.57.22/dispensariopazybien', 'http://165.227.57.22/dispensariopazybienback', 'http://192.168.1.50:4200', 'http://192.168.1.50', 'http://192.168.1.50:8080', 'http://192.168.1.38:8080'];
  var corsOptions = {
    origin: function origin(_origin, callback) {
      if (allowedOrigins.indexOf(_origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error('not allowed cors'));
      }
    }
  };
  app.use(_bodyParser["default"].json());
  app.use(_bodyParser["default"].urlencoded({
    extended: true
  }));
  app.use((0, _cors["default"])());
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  }); // console.log(app.db.models)
};