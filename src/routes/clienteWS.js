import ws from 'ws'

import url from 'url'

import fetch from 'node-fetch'

// SE TIENE QUE VER EL CASO EN EL QUE EL SOCKET NO ESTA ABIERTO
// SE PUEDE ESPERAR MENSAJES POR EL WEBSOCKET CON EL EVENTO:
// wssClient.on('message', callbackFunc)
export default (app, server) => {
  var socketsArray=[]
  let wssClient = new ws.Server({
    server: server,
    path: '/client'
  })

  function noop() { }

  function heartbeat() {
    this.isAlive = true
  }

  setInterval(function ping() {
    wssClient.clients.forEach(ws => {
      if (!ws.isAlive) {
        let socketTodos = Object.values(userSockets)
        let arrIds = Object.keys(userSockets)
        let idx = socketTodos.indexOf(ws)
        let idxSocket = arrIds[idx]
        delete userSockets[idxSocket]
        console.log('socket con index ' + idx + 'fue quitado de userSockets ' + Object.keys(userSockets))
        return ws.terminate()
      }
      ws.isAlive = false
      ws.ping(noop)
    })
  }, 10000)

  let userSockets = {}

  wssClient.on('connection', (socket, request) => {
    socket.isAlive = true
    socket.on('pong', heartbeat)

    const parameters = url.parse(request.url, true)
    const userId = parameters.query.user_id
    userSockets[userId] = socket
    console.log('Conexion establecida para usuario', userId)
    socketsArray.push(userId)
  })

    app.route('/notificacion/:id/:hcl')
    .get((req, res) => {
      const hcl = req.params.hcl
      const id = req.params.id
      console.log('api llamada')
      console.log(hcl)
      // let socket = userSockets[id]
      console.log(socketsArray.length)

      for (let i = 0; i < socketsArray.length; i++) {
        let socket = userSockets[socketsArray[i]]
        if (typeof (socket) !== 'undefined' && socket.readyState === ws.OPEN) {
          socket.send(JSON.stringify({hcl:hcl}))
          console.log("enviado a:"+i)
          console.log(socket)
          res.json({hola:'holaa'})
        } else {     
          res.json({hola:'error'})   
          res.status(204).json({ mensaje: 'exito' })
        }
      }
      // userSockets.forEach(data => {
      //   if (typeof (data) !== 'undefined' && data.readyState === ws.OPEN) {
      //     data.send(JSON.stringify({hcl:hcl}))
      //     // userSockets.forEach(data => {
      //     //   data.send(JSON.JSON.stringify({hcl:hcl}))
      //     // });
      //     res.json({hola:'holaa'})
      //   } else {     
      //     res.json({hola:'error'})   
      //     res.status(204).json({ mensaje: 'exito' })
      //   }
      // });
      
    }) 

}