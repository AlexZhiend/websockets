import bodyParser from 'body-parser'
import cors from 'cors'

module.exports = app => {
  let allowedOrigins = ['localhost:8080','http://165.227.57.22','http://165.227.57.22/dispensariopazybien','http://165.227.57.22/dispensariopazybienback','http://192.168.1.50:4200','http://192.168.1.50', 'http://192.168.1.50:8080','http://192.168.1.38:8080']
  let corsOptions = {
    origin: (origin, callback) => {
      if (allowedOrigins.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('not allowed cors'))
      }
    }
  }
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(cors())

  app.use((err, req, res, next) => {
 
    res.status(err.status || 500)
    res.json({
      message: err.message,
      error: err
    })
  })
  // console.log(app.db.models)
}
